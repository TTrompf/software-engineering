﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rechnen
{
    public class Rechner
    {
        public Double Ausrechnen(Double a, Double b, char op)

        {
            Double erg = 0;
            switch (op)
            {
                case '+': erg = a + b; break;
                case '-': erg = a - b; break;
                case 'x': erg = a * b; break;
                case '/': erg = a / b; break;
                default: erg = 0.0; break;
 
            }
            return erg;
        }
    }
}

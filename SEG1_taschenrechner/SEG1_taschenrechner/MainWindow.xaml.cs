﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace SEG1_taschenrechner
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Berechnen_Click(object sender, RoutedEventArgs e)
        {
            Rechnen.Rechner r = new Rechnen.Rechner();
            Double erg = r.Ausrechnen(
                System.Convert.ToDouble(Wert1.Text),
                System.Convert.ToDouble(Wert2.Text),
                Box_operation.Text[0]);
            Ergebniss.Text = erg.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://fh-westkueste.de");
            System.Threading.Thread.Sleep(3000);
                System.Diagnostics.Process.Start("chrome.exe","http://hsv.de");
                Process[] processNames = Process.GetProcessesByName("firefox");
                System.Threading.Thread.Sleep(3000);
                foreach (Process item in processNames)
                {
                    item.Kill();
                }
            App.Current.Shutdown();
            }
        }
    }

